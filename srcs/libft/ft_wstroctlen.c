/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_wstroctlen.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/12 20:54:47 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/13 17:49:18 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

int			ft_wstroctlen(wchar_t *wstr)
{
	int		i;
	int		j;

	j = 0;
	i = -1;
	if (!wstr)
		return (j);
	while (wstr[++i])
	{
		if (wstr[i] <= 127)
			j++;
		else if (wstr[i] <= 2047)
			j += 2;
		else if (wstr[i] <= 65535)
			j += 3;
		else if (wstr[i] <= 1114111)
			j += 4;
	}
	return (j);
}
