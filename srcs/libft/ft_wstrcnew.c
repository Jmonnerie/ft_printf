/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_wstrcnew.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/04/12 16:47:27 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/12 16:51:15 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

wchar_t		*ft_wstrcnew(size_t size, char c)
{
	wchar_t		*wstr;
	size_t		i;

	i = 0;
	if (!(wstr = (wchar_t *)malloc(sizeof(wchar_t) * size + 1)))
		return (NULL);
	while (i < size)
		wstr[i++] = (wchar_t)c;
	wstr[i] = 0;
	return (wstr);
}
