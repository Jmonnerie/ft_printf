/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_getcns.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/24 08:22:29 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/14 20:34:14 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static int	ft_check_locale(wchar_t c, t_printf *lst, int i)
{
	if (MB_CUR_MAX == 1)
	{
		if (c > 127 && c < 256)
		{
			lst->wstr[i] = (char)c;
			return (0);
		}
		return ((c < 0 || c > 127 || (c >= 0xD800 && c <= 0xDFFF)) ? -1 : 0);
	}
	else if (MB_CUR_MAX == 2)
		return ((c < 0 || c > 2047 || (c >= 0xD800 && c <= 0xDFFF)) ? -1 : 0);
	else if (MB_CUR_MAX == 3)
		return ((c < 0 || c > 65535 || (c >= 0xD800 && c <= 0xDFFF)) ? -1 : 0);
	else if (MB_CUR_MAX == 4)
	{
		return ((c < 0 || c > 1114111 || (c >= 0xD800 && c <= 0xDFFF)) ?
				-1 : 0);
	}
	return (-1);
}

static int	ft_check_last_char(t_printf *lst, int i)
{
	int		len;
	int		j;

	j = -1;
	len = lst->prec;
	while (++j < i)
	{
		if (lst->wstr[j] <= 127)
			len--;
		else if (lst->wstr[j] <= 2047)
			len -= 2;
		else if (lst->wstr[j] <= 65535)
			len -= 3;
		else if (lst->wstr[j] <= 1114111)
			len -= 4;
	}
	if (len < 0 || (len > 0 && ft_check_locale(lst->wstr[j], lst, j) == -1))
		return (0);
	return (1);
}

static int	ft_wprec(t_printf *lst, int len, int i, wchar_t *tmp)
{
	while (lst->prec > len)
	{
		if (lst->wstr[i] <= 127 && lst->prec >= len + 1)
			len++;
		else if (lst->wstr[i] <= 2047 && lst->prec >= len + 2)
			len += 2;
		else if (lst->wstr[i] <= 65535 && lst->prec >= len + 3)
			len += 3;
		else if (lst->wstr[i] <= 1114111 && lst->prec >= len + 4)
			len += 4;
		else
			break ;
		i++;
	}
	if (!ft_check_last_char(lst, i))
		return (-1);
	if (!(tmp = (wchar_t *)malloc(sizeof(wchar_t) * i + 1)))
		return (-1);
	tmp[i] = '\0';
	while (--i >= 0)
		tmp[i] = lst->wstr[i];
	free(lst->wstr);
	lst->wstr = tmp;
	return (0);
}

static int	ft_check_unicode(t_printf *lst)
{
	int		i;

	i = -1;
	if (lst->type == 'C')
		return (ft_check_locale(lst->wstr[0], lst, 0));
	if (lst->type == 'S')
	{
		if (!lst->wstr)
			lst->wstr = ft_wstrdup(L"(null)");
		else
		{
			if ((lst->prec != 0) && lst->prec < ft_wstroctlen(lst->wstr))
			{
				lst->prec = lst->prec == -1 ? 0 : lst->prec;
				if (ft_wprec(lst, 0, 0, NULL) == -1)
					return (-1);
			}
			while (lst->wstr[++i])
				if (ft_check_locale(lst->wstr[i], lst, i) == -1)
					return (-1);
		}
	}
	return (0);
}

void		ft_getcns(va_list ap, t_printf *lst)
{
	char	c;

	if ((lst->type == 'c' || lst->type == 's') && lst->length == 'l')
		lst->type = (lst->type == 'c' ? 'C' : 'S');
	if (lst->type == 'c')
	{
		c = (char)va_arg(ap, long long int);
		if (!(lst->str = ft_chartostr(c)))
			return ;
	}
	else if (lst->type == 's')
		lst->str = (char *)va_arg(ap, unsigned long long int);
	else if (lst->type == 'C')
		lst->wstr = ft_wchartowstr(va_arg(ap, wchar_t));
	else if (lst->type == 'S')
		lst->wstr = ft_wstrdup(va_arg(ap, wchar_t *));
	if (!lst->str)
		lst->str = ft_strdup("(null)");
	else if (lst->prec == -1)
		lst->str = ft_strdup("");
	else
		lst->str = ft_strdup(lst->str);
	lst->error = ft_check_unicode(lst);
}
