/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmphex.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/02/20 18:15:01 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/03/16 17:04:55 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static int		ft_calcprinthex(t_printf *lst)
{
	int		len;

	if (lst->prec >= (lst->slen = ft_strlen(lst->str)))
	{
		if ((len = lst->prec + lst->flag[0]) <= lst->width)
			return (lst->plen = lst->width);
		return (lst->plen = len);
	}
	else
	{
		len = lst->slen;
		if ((len += lst->flag[0]) <= lst->width)
			return (lst->plen = lst->width);
		return (lst->plen = len);
	}
}

static void		ft_cmphex2(t_printf *lst, int i, int j)
{
	int	k;

	k = (lst->print[0] == '0' ? 0 : i);
	if (lst->flag[0])
	{
		lst->print[k++] = '0';
		lst->print[k++] = (lst->type == 'X' ? 'X' : 'x');
		i += 2;
	}
	if (lst->prec > lst->slen)
	{
		while (++j < lst->prec)
			lst->print[i + j] = '0';
		i += j - lst->slen;
		j = -1;
	}
	while (lst->str[++j])
		lst->print[i + j] = lst->str[j];
}

void			ft_cmphex(t_printf *lst)
{
	int		i;

	i = 0;
	if (!(lst->print = ft_strcnew(ft_calcprinthex(lst), ' ')))
		return ;
	if (lst->prec == 0 && lst->flag[4] && !lst->flag[1])
		while (lst->print[i])
			lst->print[i++] = '0';
	if (!lst->flag[1])
	{
		i = lst->plen - lst->flag[0];
		i -= (lst->slen > lst->prec ? lst->slen : lst->prec);
	}
	else
		i = 0;
	ft_cmphex2(lst, i, -1);
}
