/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmpbin.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/03/17 06:37:22 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/03/17 10:35:48 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static int	ft_calcprintbin(t_printf *lst)
{
	lst->slen = ft_strlen(lst->str) - lst->neg;
	if (lst->prec >= lst->slen)
		return (lst->plen = (lst->prec <= lst->width ? lst->width : lst->slen));
	else
		return (lst->plen = (lst->slen <= lst->width ? lst->width : lst->slen));
}

void		ft_cmpbin(t_printf *lst)
{
	int		i;
	int		j;

	j = -1;
	i = 0;
	if (!(lst->print = ft_strcnew(ft_calcprintbin(lst), ' ')))
		return ;
	if (lst->prec == 0 && lst->flag[4] && !lst->flag[1])
		while (lst->print[i])
			lst->print[i++] = '0';
	i = (lst->prec > lst->slen ? lst->prec : lst->slen);
	i = (lst->flag[1] ? 0 : lst->plen - i);
	if (lst->prec > lst->slen)
	{
		while (++j < lst->prec)
			lst->print[i + j] = '0';
		i += j - lst->slen;
	}
	j = -1;
	while (lst->str[++j])
		lst->print[i++] = lst->str[j];
}
