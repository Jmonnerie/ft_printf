/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_precision.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/16 01:41:34 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/03/16 19:05:47 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

void	ft_precision(t_printf *lst, char **str, va_list ap)
{
	*str += 1;
	if (**str >= '0' && **str <= '9')
	{
		lst->prec = ft_atoi(*str);
		if (lst->prec == 0)
			lst->prec = -1;
	}
	else if (**str == '*')
	{
		lst->prec = va_arg(ap, long long int);
		if (lst->prec == 0)
			lst->prec = -1;
		else if (lst->prec < 0)
			lst->prec = 0;
	}
	else
		lst->prec = -1;
	if (**str != '*')
		while (**str <= '9' && **str >= '0')
			*str += 1;
	else
		*str += 1;
}
