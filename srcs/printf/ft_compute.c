/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_compute.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/18 17:06:06 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/14 19:45:17 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

void	ft_compute(t_printf *lst)
{
	if (ft_strchr("UuDdi", lst->type))
		ft_cmpint(lst);
	else if (ft_strchr("Xxp", lst->type))
		ft_cmphex(lst);
	else if (ft_strchr("Oo", lst->type))
		ft_cmpoct(lst);
	else if (lst->type == '%')
		ft_cmpercent(lst);
	else if (ft_strchr("cCsS", lst->type))
		ft_cmpchars(lst);
	else if (lst->type == 'b')
		ft_cmpbin(lst);
}
