/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmpint.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/26 10:56:09 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/03/16 17:06:25 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static int	ft_calcprintint(t_printf *lst)
{
	int	len;

	lst->slen = ft_strlen(lst->str) - lst->neg;
	if (lst->prec >= lst->slen)
	{
		if ((len = lst->prec + (lst->flag[2] || lst->flag[3] || lst->neg)) <=
				lst->width)
			return (lst->plen = lst->width);
		return (lst->plen = len);
	}
	else
	{
		len = lst->slen;
		if ((len += (lst->flag[2] || lst->flag[3] || lst->neg)) <= lst->width)
			return (lst->plen = lst->width);
		return (lst->plen = len);
	}
}

static void	ft_cmpint2(t_printf *lst, int i, int j)
{
	if (lst->neg)
	{
		lst->print[(lst->print[0] == '0' ? 0 : i)] = '-';
		i++;
	}
	else if (lst->flag[2])
	{
		if (lst->flag[4] && lst->prec == 0)
			lst->print[0] = '+';
		else
			lst->print[i] = '+';
		i++;
	}
	else if (lst->flag[3])
		lst->print[0] = ' ';
	if (lst->prec > lst->slen)
	{
		while (++j < lst->prec)
			lst->print[i + j] = '0';
		i += j - lst->slen;
	}
	j = lst->neg - 1;
	while (lst->str[++j])
		lst->print[i++] = lst->str[j];
}

void		ft_cmpint(t_printf *lst)
{
	int	i;

	i = 0;
	if (lst->type == 'u' || lst->type == 'U')
	{
		lst->flag[2] = 0;
		lst->flag[3] = 0;
	}
	if (!(lst->print = ft_strcnew(ft_calcprintint(lst), ' ')))
		return ;
	if (lst->prec == 0 && lst->flag[4] && !lst->flag[1])
		while (lst->print[i])
			lst->print[i++] = '0';
	if (!lst->flag[1])
	{
		i = lst->plen - (lst->flag[2] || lst->neg);
		i -= (lst->slen > lst->prec ? lst->slen : lst->prec);
	}
	else
		i = 0;
	ft_cmpint2(lst, i, -1);
}
