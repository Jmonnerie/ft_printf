/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_printf.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/11 16:01:48 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/12 17:34:19 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static int	ft_return_engine(int eng, int *ret)
{
	if (eng == -1)
		return (0);
	*ret += eng;
	return (1);
}

static int	ft_buffing(char *str, int ret, va_list ap)
{
	char	*tmp;

	while (*str)
	{
		tmp = str;
		while (*str && *str != '%' && *str != '{')
			str++;
		if (*str == '%')
		{
			if (!ft_return_engine(ft_engine(tmp, str - tmp, &str, ap), &ret))
				return (-1);
		}
		else if (*str == '{')
		{
			ret += write(1, tmp, str - tmp);
			ret += ft_colors(&str);
		}
		else
		{
			ret += write(1, tmp, str - tmp);
			return (ret);
		}
		str++;
	}
	return (ret);
}

int			ft_printf(const char *restrict format, ...)
{
	va_list	ap;
	char	*str;
	int		ret;

	ret = 0;
	str = (char *)format;
	va_start(ap, format);
	ret = ft_buffing(str, ret, ap);
	va_end(ap);
	return (ret);
}
