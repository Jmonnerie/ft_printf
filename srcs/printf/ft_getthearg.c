/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_getthearg.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/18 18:33:37 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/14 19:22:49 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static char	*ft_itoxou(unsigned long long int i, t_printf *lst)
{
	if ((lst->type == 'x' || lst->type == 'X') && lst->flag[0])
		lst->flag[0] = (i == 0 ? 0 : 2);
	if (lst->prec == -1 && i == 0)
		return (ft_strnew(0));
	if (lst->type == 'X')
		return (ft_ulitoa_base(i, 16));
	else if (lst->type == 'x')
		return (ft_strtolower(ft_ulitoa_base(i, 16)));
	else if (lst->type == 'O' || lst->type == 'o')
	{
		if (i == 0)
			lst->flag[0] = 0;
		return (ft_ulitoa_base(i, 8));
	}
	else
		return (ft_ulitoa(i));
}

static void	ft_xou(va_list ap, t_printf *lst)
{
	if (lst->type == 'U' && (lst->length == 'H' || lst->length == 'h' ||
				lst->length == 0))
	{
		if (lst->length == 'H')
			lst->str = ft_itoxou((unsigned short)va_arg(ap, long int), lst);
		else
			lst->str = ft_itoxou(va_arg(ap, unsigned long int), lst);
	}
	else if (lst->length == 'l' || lst->type == 'O')
		lst->str = ft_itoxou(va_arg(ap, unsigned long int), lst);
	else if (lst->length == 0)
		lst->str = ft_itoxou(va_arg(ap, unsigned int), lst);
	else if (lst->length == 'H')
		lst->str = ft_itoxou((unsigned char)va_arg(ap, long long int), lst);
	else if (lst->length == 'h')
		lst->str = ft_itoxou((unsigned short)va_arg(ap, long long int), lst);
	else if (lst->length == 'L')
		lst->str = ft_itoxou(va_arg(ap, unsigned long long), lst);
	else if (lst->length == 'j')
		lst->str = ft_itoxou(va_arg(ap, UINTMAX), lst);
	else
		lst->str = ft_itoxou(va_arg(ap, size_t), lst);
}

static void	ft_integer(va_list ap, t_printf *lst)
{
	long long int i;

	i = va_arg(ap, long long int);
	if (!i && lst->prec == -1)
		lst->str = ft_strnew(0);
	else if (lst->type == 'D' || lst->length == 'l')
		lst->str = ft_litoa((long int)i);
	else if (lst->length == 0)
		lst->str = ft_litoa((int)i);
	else if (lst->length == 'L')
		lst->str = ft_litoa((long long int)i);
	else if (lst->length == 'h')
		lst->str = ft_litoa((short int)i);
	else if (lst->length == 'H')
		lst->str = ft_litoa((char)i);
	else if (lst->length == 'j')
		lst->str = ft_litoa((INTMAX)i);
	else
		lst->str = ft_litoa((size_t)i);
	if (lst->str[0] == '-')
		lst->neg = 1;
}

void		ft_getthearg(t_printf *lst, va_list ap)
{
	unsigned long int tmp;

	tmp = 0;
	free(lst->str);
	if (ft_strchr("XxOoUu", lst->type))
		ft_xou(ap, lst);
	else if (ft_strchr("Ddi", lst->type))
		ft_integer(ap, lst);
	else if (lst->type == 'p')
	{
		tmp = va_arg(ap, unsigned long);
		if (tmp == 0 && lst->prec == -1)
			lst->str = ft_strdup("");
		else
			lst->str = ft_strtolower(ft_ulitoa_base(tmp, 16));
		lst->flag[0] = 2;
	}
	else if (lst->type == 'b')
		lst->str = ft_litoa_base(va_arg(ap, long long int), 2);
	else if (lst->type == '%')
		lst->str = ft_strdup("%");
	else
		ft_getcns(ap, lst);
}
