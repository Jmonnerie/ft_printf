/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_cmpoct.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/02/24 18:45:34 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/03/17 03:21:53 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "../../incs/libft.h"

static int		ft_calcprintoct(t_printf *lst)
{
	int		len;

	if (lst->prec > (lst->slen = ft_strlen(lst->str)))
	{
		if (lst->prec <= lst->width)
			return (lst->plen = lst->width);
		return (lst->plen = lst->prec);
	}
	else
	{
		len = lst->slen;
		if ((len += lst->flag[0]) <= lst->width)
			return (lst->plen = lst->width);
		return (lst->plen = len);
	}
}

static void		ft_cmpoct2(t_printf *lst, int i)
{
	int		j;

	j = -1;
	if (lst->flag[0] && lst->prec <= lst->slen)
		lst->print[i++] = '0';
	if (lst->prec > lst->slen)
	{
		while (++j < lst->prec)
			lst->print[i + j] = '0';
		i += j - lst->slen;
		j = -1;
	}
	while (lst->str[++j])
		lst->print[i + j] = lst->str[j];
}

void			ft_cmpoct(t_printf *lst)
{
	int		i;

	i = 0;
	if (!(lst->print = ft_strcnew(ft_calcprintoct(lst), ' ')))
		return ;
	if (lst->prec == 0 && lst->flag[4] && !lst->flag[1])
		while (lst->print[i])
			lst->print[i++] = '0';
	if (!lst->flag[1])
		i = lst->plen - (lst->slen >= lst->prec ? lst->slen +
				lst->flag[0] : lst->prec);
	else
		i = 0;
	ft_cmpoct2(lst, i);
}
