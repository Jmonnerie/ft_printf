/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   test.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/01/15 17:52:46 by jmonneri     #+#   ##    ##    #+#       */
/*   Updated: 2018/04/14 20:42:31 by jmonneri    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "incs/libft.h"
#include <locale.h>

int		main(void)
{
	int		i;
	wchar_t s[4];
	char	*str;

	str = ft_strdup("|%d|");
	setlocale(LC_ALL, "");
	s[0] = 'S';
	s[1] = 254;
	s[2] = 'u';
	s[3] = '\0';
	printf("------ PRINTF RETURN\n");
	i = printf(str, 42);
	printf("\nReturn : %d\n", i);
	dprintf(1, "------ FT_PRINTF RETURN\n");
	i = ft_printf(str, 42);
	printf("\nReturn : %d\n", i);
	return (0);
}
