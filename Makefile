# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: jmonneri <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2017/12/14 19:15:23 by jmonneri     #+#   ##    ##    #+#        #
#    Updated: 2018/04/14 20:40:14 by jmonneri    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

.PHONY: set_macos set_linux all clean fclean re

NAME = libftprintf.a
CC = gcc
CC_FLAGS = -Wall -Wextra -Werror
PATH_OBJ = ./objs/
PATH_SRC = ./srcs/
PATH_INC = ./incs/
INC = $(addprefix $(PATH_INC), libft.h)

#******************************************************************************#
#                                    LIBFT                                     #
#******************************************************************************#

PATH_SRC_LIBFT = $(PATH_SRC)libft/
PATH_OBJ_LIBFT = $(PATH_OBJ)libft/
FILES_LIBFT = ft_memset ft_bzero ft_memcpy ft_memccpy ft_memmove ft_memcmp\
		ft_memchr ft_strncat ft_strlcat ft_strcat ft_strncpy ft_strcpy\
		ft_strdup ft_strlen ft_putchar ft_putstr ft_putnbr ft_strchr ft_strrchr\
		ft_strstr ft_strnstr ft_strcmp ft_strncmp ft_atoi ft_isalpha ft_isdigit\
		ft_isalnum ft_isascii ft_isprint ft_toupper ft_tolower ft_memalloc\
		ft_memdel ft_strnew ft_strdel ft_strclr ft_striter ft_striteri\
		ft_strmap ft_strmapi ft_strequ ft_strnequ ft_strsub ft_strjoin\
		ft_strtrim ft_strsplit ft_litoa ft_litoa_base ft_putendl ft_putchar_fd\
		ft_putstr_fd ft_putendl_fd ft_putnbr_fd ft_lstnew ft_lstdelone\
		ft_lstdel ft_lstadd ft_lstiter ft_lstmap ft_lintlen ft_print_strlst\
		ft_putstr2d ft_freestr2d ft_strtolower ft_strtolower ft_chartostr\
		ft_ulitoa ft_ulitoa_base ft_ulintlen ft_strcnew ft_wchartowstr\
		ft_wstrdup ft_wstrlen ft_putwchar ft_putwstr ft_wchar_size ft_wstrcnew\
		ft_wstroctlen
OBJ_LIBFT = $(addprefix $(PATH_OBJ_LIBFT), $(addsuffix .o, $(FILES_LIBFT)))
SRC_LIBFT = $(addprefix $(PATH_SRC_LIBFT), $(addsuffix .c, $(FILES_LIBFT)))

#******************************************************************************#
#                                    PRINTF                                    #
#******************************************************************************#

PATH_SRC_PRINTF = $(PATH_SRC)printf/
PATH_OBJ_PRINTF = $(PATH_OBJ)printf/
FILES_PRINTF = $(shell ls srcs/printf | cut -d "." -f 1)
OBJ_PRINTF = $(addprefix $(PATH_OBJ_PRINTF), $(addsuffix .o, $(FILES_PRINTF)))
SRC_PRINTF = $(addprefix $(PATH_SRC_PRINTF), $(addsuffix .c, $(FILES_PRINTF)))

#******************************************************************************#
#                                     ALL                                      #
#******************************************************************************#

PATHS_OBJ = $(PATH_OBJ) $(PATH_OBJ_LIBFT) $(PATH_OBJ_PRINTF)
OBJ = $(OBJ_LIBFT) $(OBJ_PRINTF)
SRC = $(SRC_LIBFT) $(SRC_PRINTF)
FILES = $(FILES_LIBFT) $(FILES_PRINTF)

#******************************************************************************#
#                                    RULES                                     #
#******************************************************************************#

all: $(NAME)

test: all test.c
	@printf "Exe compilation : "
	@$(CC) test.c -L ./ -lftprintf -I $(PATH_INC)
	@printf "\033[0;32m[OK]\033[0m\n"
	@./a.out

teste: all test.c
	@printf "Exe compilation : "
	@$(CC) test.c -L ./ -lftprintf -I $(PATH_INC)
	@printf "\033[0;32m[OK]\033[0m\n"
	@./a.out | cat -e

curqui: all
	@cp $(NAME) ../curqui_test/
	@make -C ../curqui_test/
	@../curqui_test/ft_printf_tests

set_macos:
	@vim $(INC) -c "%s/# define LINUX/# define MACOS/g" -c :x

set_linux:
	@vim $(INC) -c "%s/# define MACOS/# define LINUX/g" -c :x

clean:
	@printf "\n\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DES OBJETS🦄 🦄 🦄 🦄\033[0m\n"
	@rm -rf $(PATH_OBJ)

fclean: clean
	@printf "\n\033[1m🦄 🦄 🦄 🦄 SUPPRESSION DE $(NAME)🦄 🦄 🦄 🦄\033[0m\n"
	@rm -f $(NAME)
	@rm -rf a.out
	@rm -rf rslt_trace.txt

re: fclean all

#******************************************************************************#
#                                 Comp LIBFT                                   #
#******************************************************************************#

$(NAME): $(PATHS_OBJ) $(OBJ)
	@printf "\n\033[1m🦄 🦄 🦄 🦄 CREATION DE LA LIBFTPRINTF🦄 🦄 🦄 🦄\033[0m\n"
	@ar rcs $(NAME) $(OBJ)
	@printf "  👍  👍  👍 \033[1mLIBRAIRIE CREEE\033[0m👍  👍  👍\n\n"

$(PATH_OBJ)%.o: $(PATH_SRC)%.c $(INC)
	@printf %b "0️⃣  Compilation de \033[1m$<\033[0m en \033[1m$@\033[0m"
	@$(CC) $(CC_FLAGS) -o $@ -c $< -I $(PATH_INC)
	@printf %b "\r                                                                                   \r"

$(PATHS_OBJ):
	@mkdir $@
